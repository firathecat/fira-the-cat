# Aktualności

## Po zdjęciu szwów

_12 października 2021 r._

Długo nie pisałem, bo ile można pisać o tym, że kotu puszczają szwy ;) A w międzyczasie, to znaczy
od ostatniej niedzieli, szwy puściły już w czwartek (bodaj!) - tym razem trzymające trzecią powiekę
w ryzach. Po konsultacji z weterynarzem (tym na urlopie) podjęto decyzję o odpuszczeniu szycia, skoro
i tak po kilku dniach Fira miała wrócić na zdjęcie szwów.

No i dzisiaj zostały zdjęte. **Fira widzi, wzrok został zachowany, rogówka wygoiła się**. 

Na oku widoczne jest przebarwienie, które przez następne dwa tygodnie będzie leczone dalej - 
deksametazonem i kwasem hialuronowym (weterynarz popatrzył na moje zmęczone oczy i stwierdził, żebym
sobie kupił taki sam preparat tylko silniejszy ;) ). 

Za dwa tygodnie konsultacja. Póki co, Fira musi jeszcze chodzić w kołnierzu, póki nie wygoją się rany
po szwach, natomiast wszystko ewidentnie zmierza ku dobremu.

PS Przez te (prawie) trzy tygodnie Fira przybrała na wadze 5% i obecnie stanowi 4,2 kg uberprzyjaznego,
nadatencyjnego, przetowarzyskiego futrzaka.

PS2 Została też samobieżnym tagiem RFID, wpisanym jako "nieagresywny kot" (dla niewtajemniczonych -
została zaczipowana).

## Fira-Szwy 2:0

_3 października 2021 r._

Firze znowu (jak to w każdą niedzielę) puściły szwy, więc pojechała do lecznicy i (znowu) będzie
miała operację. Nie wiadomo czy znudził jej się kolor szwów czy po prostu lubi ketaminę/morfinę,
ale fakt faktem, do odbioru będzie wieczorem.

PS. Widok weterynarza, ze strzykawką z ketaminą w zębach, nucącego Marsz Imperialny podczas badania
kociego oka, rekompensuje wszystko.

## Jak zdjąć kołnierz na 1001 sposobów?

_29 września 2021 r._

Każdego dnia Fira zaskakuje mnie swoimi umiejętnościami w zakresie zdejmowania kołnierzy,. Wczoraj
udało jej się go zdjąć przez tyłek (rano), wieczorem założyła go sobie na głowę jak fez, a dzisiaj
rano, z kolei, służył za śliniaczek. Na szczęście powieki zostały zszyte jak dla smoka.

## Kontrola po operacji

_28 września 2021 r._

Kontrola po operacji nie wykazała szczególnych problemów, z wyjątkiem tego, że Fira **jednak**
nie za bardzo lubi mierzenie temperatury ;)

## Potencjalny właściciel

_28 września 2021 r._

Znalazłem ogłoszenie o zagubionej kotce, w zaskakującym stopniu podobnej do Firy, która zaginęłą
ponad półtora roku temu ponad 50 kilometrów stąd. Próbowałem skontaktować się z osobą, która
zamieściła ogłoszenie - niestety bezskutecznie. Dodatkowo, po konsultacji zdalnej z lekarzem
weterynarii (nieoficjalnej!) okazało się, że to najpewniej dwa różne koty. Oprócz tego, powiadomiłem
Schronisko dla Zwierząt "Na Paluchu" oraz Straż Miejską m.st. Warszawy (co jest obowiązkiem znalazcy
rzeczy bądź zwierzęcia).

Przy okazji - Fira wie, jak poradzić sobie nawet z najbardziej wymyślnym kołnierzem...

## Powrót z kliniki

_27 września 2021 r._

Fira wróciła do mojego domu. Operacja się udała, ale dzisiaj musi (raczej) zostać w transporterze,
żeby nie uszkodzić (znowu!). Jutro kolejna kontrola. 

Niestety, jedna z kotek-rezydentek słabo znosi stres, spowodowany zmianą w mieszkaniu i wylizuje
sobie sierść. Jutro będziemy próbować z obrożą walerianową, na razie uspokaja się w cichym i ciemnym
miejscu (spokojnie, jest bezpieczna!).

## Operacja udana

_27 września 2021 r._

Telefonicznie zostałem poinformowany, że operacja zakończyła się sukcesem. Firę odbieram wieczorem,
żeby zdążyła się odpowiednio wybudzić i żeby była pewnosć, że jeśli odstawi jakiś numer świeżo po
zabiegu, od razu się nią zajmą.

## Kolejna operacja

_26 września 2021 r._

Szwy utrzymujące powieki zewnętrzne w pozycji zamkniętej puściły i powieki otworzyły się w 100%.
Na szczęście szwy utrzymujące wewnętrzną, trzecią powiekę, trzymają ją mocno. Okulista, operujący
Firę, mimo urlopu, zdalnie konsultował stan Firy i zadecydował o konieczności zaszycia powiek - podczas
gojenia się, operowane oko nie powinno być wystawione na zmiany temperatury i wilgotności. Naturalny
opatrunek z własnych powiek kota stanowi idealne zabezpieczenie przed warunkami atmosferycznymi.

Fira została na noc w klinice i o 8:30 przejdzie kolejną operację.

Rana na boku goi się natomiast bez większych komplikacji.

---

## Fira przeprowadza się na dobę

_25 września 2021 r._

Fira na jedną dobę trafi do Gabrysi, ponieważ w moim domu muszę ogarnąć trochę rzeczy, aby cztery
koty, w tym trzy rezydentki, mogły koegzystować tak spokojnie, jak to możliwe.

---

## Cały czas dobrze

_24 września 2021 r._

Póki co, zarówno rana na boku, jak i oko, goją się bezproblemowo.

---

## Jest coraz lepiej

_23 września 2021 r._

Póki co, zarówno rana na boku, jak i oko, goją się bezproblemowo.

---

## Niespokojna noc

_23 września 2021 r._

Kilka godzin po operacji, Fira musiała dostać gabapentynę na uspokojenie (cały czas przebywała
w transporterku). Niestety, i to nie pomogło, i Fira musiała pojechać do kliniki na zastrzyk
uspokajający.

---

## Po operacji

_22 września 2021 r._

(...)

---

## Konsultacja u okulisty

_22 września 2021 r._

(...)

---

## Po pierwszej wizycie

_21 września 2021 r._

Czipa brak, głęboka rana na boku, a także zadrapanie w oku. Rana została zabezpieczona, kot
dostał leki, następnego dnia idziemy na konsultację o okulisty.

---

## Kotka znaleziona

_21 września 2021 r._

Przed wejściem na klatkę schodową, podeszła do mnie kotka z objawami rui, ocierała się i pozwoliła
wziąć na ręce. W pierwszej kolejności ją kota na ziemię, zobaczyłem jednak, że niespokojna 
krąży w promieniu kilkudziesięciu metrów, nie potrafi zabezpieczyć się przed psami oraz 
nadjeżdżającymi samochodami, a także nie wyglądała na kota wychodzącego.

Kot zabezpieczony w domu, jedziemy do weterynarza.
